# Qogita

## Running the app.

*Please ensure you have upgraded your pip, or the `cryptography` library will complain about missing `rust`*

1. Run `python3 manage.py migrate` to build your initial database
2. Run `python3 manage.py create_default_countries` to add a few default countries to the DB. I could have added this to the migration for `address` but I preferred the visibility of an explicit management command.
3. Run `python3 manage.py add_dummy_data` to start populating your database with data for testing.
4. Run `python3 manage.py runserver` to start the server.

On #3, note that it accepts two optional positional parameters: `username` and `number of addresses`. This is to allow adding more addresses to an existing user easily during your testing.  All the users are created with `staff` and `superuser` access to allow you to log into `/admin/` and add more countries or anything else you would like to do. In effect this lets you enter  `python3 manage.py add_dummy_data steven 200` and create an account with the username/password `steven`/`a` that has 200 addresses. To add another 50, you would type in  `python3 manage.py add_dummy_data steven 50` and so on. In a real environment I would add a check for `DEBUG=True` or something to avoid this being accidentally triggered in prod.

## API

We have three key API endpoints: `auth` (login), `logout` and `addresses`. All three can be found at `/api/v1/`.

1. The auth endpoint has no access restriction
2. The logout and addresses endpoint requires authentication

Generally, all endpoints can be hit with an `OPTIONS` and it should return back meaningful information on what is and what isn't required and what each fields represents. For example, hitting the `address` endpoint with `OPTIONS` would return the following:

```
{
    "name": "Address Viewset List",
    "description": "",
    "renders": [
        "application/json",
        "text/html"
    ],
    "parses": [
        "application/json",
        "application/x-www-form-urlencoded",
        "multipart/form-data"
    ],
    "actions": {
        "POST": {
            "id": {
                "type": "integer",
                "required": false,
                "read_only": true,
                "label": "Id"
            },
            "addressLine": {
                "type": "string",
                "required": false,
                "read_only": true,
                "label": "Addressline",
                "help_text": "A one-line Australian address format of the address"
            },
            "buildingName": {
                "type": "string",
                "required": false,
                "read_only": false,
                "label": "Buildingname",
                "help_text": "Name of the building, if any"
            },
            "unitNumber": {
                "type": "string",
                "required": false,
                "read_only": false,
                "label": "Unitnumber",
                "help_text": "Unit number of the address (i.e A, B), if any"
            },
            "streetNumber": {
                "type": "string",
                "required": false,
                "read_only": false,
                "label": "Streetnumber",
                "help_text": "Street number of the address (i.e 104, 203), if any"
            },
            "streetType": {
                "type": "string",
                "required": false,
                "read_only": false,
                "label": "Streettype",
                "help_text": "The type of street (Place, Street, Road)"
            },
            "streetName": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Streetname",
                "help_text": "Name of the street for this address"
            },
            "suburb": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Suburb",
                "help_text": "The name of the local government area"
            },
            "state": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "State",
                "help_text": "The province, state, department or similar for this location"
            },
            "country": {
                "type": "field",
                "required": true,
                "read_only": false,
                "label": "Country"
            },
            "postcode": {
                "type": "string",
                "required": true,
                "read_only": false,
                "label": "Postcode"
            }
        }
    }
}
```

1. Authenticating

Send a `POST` payload to `/api/v1/auth/` with the required fields `username` and `password`.

Example payload:
```
{
    "username": "d1535b2fbc704a67a9c06dcf0d7650ef",
    "password": "a"
}
```

Example successful response:
```
{
    "token": "Token eca73439588f5a1117bd8ec6b39aaa46c420e73dad6ef4fafee2f31f5e963450"
}
```

Example failed auth response:
```
{
    "detail": "Invalid login credentials, please check your username and password"
}
```

2. Logging out


Send a `POST` request to `/api/v1/logout/`.

Example successful response:

```
[
    "Logged out successfully"
]
```

Example failed response:
```
{
    "detail": "Invalid token."
}
```

3. Querying for addresses

a. Listing all addresses

Send a `GET` request to `/api/v1/addresses/`.

Example successful response
```
{
    "count": 141,
    "next": "http://127.0.0.1:8000/api/v1/addresses/?page=2",
    "previous": null,
    "results": [
        {
            "id": 1,
            "addressLine": "Smith-Wallace, 9903 Isabella Rd, Stevenhaven AK 1085, United Kingdom",
            "buildingName": "Smith-Wallace",
            "unitNumber": "",
            "streetNumber": "9903",
            "streetType": "Rd",
            "streetName": "Isabella",
            "suburb": "Stevenhaven",
            "state": "AK",
            "country": "United Kingdom",
            "postcode": "1085"
        },
    ...
    ]
}
```

* The `count` indicates the total number of records
* The `next` value on the API indicates the next page as pagination is enforced with 100 items.
* The `previous` value indicates a previous page if you're not on the first paginated block.

b. Changing pages

Send a `GET` request to `/api/v1/addresses?page=N` where N is the page number you want to go to.

A successful request will return the values for that page.

An insuccessful request, if you choose a page that doesn't exist, will return a `404 Not Found` with the following payload:
```
{
    "detail": "Invalid page."
}
```

c. Filtering results

Send a `GET` request to `/api/v1/addresses?{attribute}={value}` where `attribute` is the model-level name of the field you want to filter for, and the value is the value you're filtering against.

The accepted values include the following:
```
'building_name',
'unit_number',
'street_number',
'street_name',
'street_type',
'suburb',
'state',
'country',
'postcode',
```

Example response for `http://127.0.0.1:8000/api/v1/addresses/?country=Netherlands`:

```
{
    "count": 29,
    "next": null,
    "previous": null,
    "results": [
        {
            "id": 10,
            "addressLine": "Thompson PLC, 3530 W Melissa Dr, New Lisaburgh ET 2016, Netherlands",
            "buildingName": "Thompson PLC",
            "unitNumber": "",
            "streetNumber": "3530 W",
            "streetType": "Dr",
            "streetName": "Melissa",
            "suburb": "New Lisaburgh",
            "state": "ET",
            "country": "Netherlands",
            "postcode": "2016"
        },
        ...
    ]
}
```

d. Deleting single addresses

Send a `DELETE` request to `/api/v1/addresses/N/` where `N` is the ID of the address you want to delete.

i. Successful deletion returns a `204 No Content`
ii. Unsuccessful deletion returns a `404 Not Found` with the following payload:
```
{
    "detail": "Not found."
}
```

e. Deleting multiple addresses

Send a `DELETE` request to `/api/v1/addresses/` with the following payload:
```
{
    "delete": [x, y, z]
}
```

Where `[x, y, z]` represents an array of address IDs you would like to delete. An array must be passed even if you only intend to delete one.

Example of failed response where an array with at least one value is not passed:
```
[
    "You must pass in an array of primary keys attached to a `delete` key"
]
```

Example of a failed response where a string or integer is passed instead of an array:
```
[
    "This endpoint can only accept arrays of primary keys"
]
```

Example of a failed response where the user is deleting addresses that do not belong to them or do not exist:
```
[
    "Invalid ID list passed in request"
]
```

Successful deletion returns a `204 No Content`.

f. Updating an address

Send a `PUT` or `PATCH` request to `/api/v1/addresses/N/` where `N` is the ID of the address you want to update.

Any keys you pass must have a value or an error will be returned, even if the keys are not required:
```
{
    "streetNumber": [
        "This field may not be blank."
    ]
}
```

Example of a successful payload:

```
{
    "buildingName": "Smith-Wallace",
    "unitNumber": "1A",
    "streetNumber": "9903",
    "streetType": "Rd",
    "streetName": "Isabella",
    "suburb": "Stevenhaven",
    "state": "AK",
    "country": "United Kingdom",
    "postcode": "1085"
}
```

Example of a successful response returns a `200 OK` along with the updated object:

```
{
    "id": 1,
    "addressLine": "Smith-Wallace, 9903 Isabella Rd, Stevenhaven AK 1085, United Kingdom",
    "buildingName": "Smith-Wallace",
    "unitNumber": "1A",
    "streetNumber": "9903",
    "streetType": "Rd",
    "streetName": "Isabella",
    "suburb": "Stevenhaven",
    "state": "AK",
    "country": "United Kingdom",
    "postcode": "1085"
}
```

g. Adding a new address

Send a `POST` request to `/api/v1/addresses/` with the payload of the address you want to create.

Example payload:
```
{
    "buildingName": "Qogita",
    "streetNumber": "4B",
    "streetType": "Rd",
    "streetName": "Isabella",
    "suburb": "Stevenhaven",
    "state": "AK",
    "country": "United Kingdom",
    "postcode": "1085"
}
```

Example of a successful response returns a `200 OK` along with the updated object:

```
{
    "id": 142,
    "addressLine": "Qogita, 4B Isabella Rd, Stevenhaven AK 1085, United Kingdom",
    "buildingName": "Qogita",
    "unitNumber": "",
    "streetNumber": "4B",
    "streetType": "Rd",
    "streetName": "Isabella",
    "suburb": "Stevenhaven",
    "state": "AK",
    "country": "United Kingdom",
    "postcode": "1085"
}
```

You cannot duplicate addresses. If you do try to create a new address or update an existing one with the same attributes, it will return the following along with a `400 Bad Request`:
```
[
    "This address already exists on your account."
]
```

## General notes

1. Generally there would be restrictions on session auth vs. token auth depending on the environment: i.e, in production we may limit it to just use Token authentication. For the purposes of this exercise, just so that the web-based DRF gui can be used, I kept session auth enabled. You can log in by going to `/admin/` and entering the credentials there, then navigating to `/api/v1/` where all endpoints should be accessible from the browser.
2. I added and used `rest-knox` for tokens rather than the default DRF Token authentication because of various benefits it introduces. My reasoning is towards the end of [this article](https://spapas.github.io/2021/08/25/django-token-rest-auth/) from last week (I didn't write it -- just reviewed it and contributed that addition) but basically comes down to offering things like the ability to add expiry times and storing hashes in the DB rather than the token. I have rolled out my own token-based system in the past (for account/email validation) but felt it was a bit too much for this exercise.
3. I wrote my own filtering methods and my own login and logout viewsets. I wouldn't normally do this, at least not as viewsets, and would usually defer to a separate auth package. At the very least, if I were to do this again, I likely wouldn't have chosen viewsets and should have done standard views, but I wanted to keep it consistent for this exercise.
4. The way bulk-delete has been implemented was quite crude - DRF doesn't like `DELETE`s happening on the `LIST` views. Perhaps a better way would be to just add `url_path=r'delete'` to the delete `action` in `address/viewsets.py` to give it its own path at `/api/v1/addresses/delete/`. It can probably be modified to just accept a direct array of values instead.
5. I made lots of assumptions about what a valid address contains. I narrowed it down to a valid address just requiring a street name, suburb, state, country and postcode, with all the other things optional. I don't think this is correct, and thinking about it more I can imagine some addresses might have multiple street numbers or multiple street names. Or maybe the right decision is just to capture them by lines (`line 1`, `line 2`, `line 3`). So there may be a better way of doing that.
6. I only used generic `serializers` and didn't use `ModelSerializers` or any other magical "easy" features to avoid abstracting away the business logic. To this end I wrote all of my own `update` and `create` methods for the serializers. I did however use various DRF mixins to try to keep the viewsets as lean as possible.
7. The way I'm handling `logout` invalidates *all* tokens for a user. This is not optimal, and if it were a production project I would only invalidate the one used to make the request. But because DRF's default token auth only allows for one token per user anyway I decided not to spend too much time there. I'm also directly deleting the tokens where the better decision might be to just invalidate them.
8. I didn't add types to my Python code although I usually lean towards doing that, and I didn't write any tests. If either of these things needed to be demonstrated please let me know and I am more than happy to write tests against the models/views/serializers within an hour. I did install `factory_boy` which I usually use for mocking models during testing, but I used it for prepopulating dummy data instead.
9. I believe filtering and pagination may clash although I haven't tested this. I would not have rolled my own "made at home" filtering in production, there is native DRF support for filtering backends and plenty of battle-tested packages that do this quite well, including extending `rest_framework.filters`. I put my own filtering to try to demonstrate that it can be done, if necessary, and I believe it provides sufficient usability for this exercise. If this is a concern please let me know and I can implement a better approach using one of the methods listed here.
10. To be audit-complete I would have considered adding something like `reversion`, otherwise it feels uncomfortable to directly delete things like addresses. At the very least I would have preferred adding `is_active` booleans and turning them on and off as required, and then querying against that. It would make things like historical invoices easier to access, and it makes more sense for the end-user to have the ability to archive addresses at least in addition to deleting them.
11. The addresses contain attributes for the datetime they were created and the datetime they were last updated. This can easily be added to the APIs to enable things like ordering/filtering based on those dates.
12. There is no input validation or cleaning (such as removing whitespace) for addresses -- you can add even add emojis as a street name if you want to. This isn't optimal, of course, but considering that addresses can be international and include letters beyond non-standard English I felt like the validation would be too much for this exercise. In a real situation I would at least call `.strip()` to remove leading and trailing whitespace.
13. The way I enforced stopping duplicates was adding a database-level uniqueness constraint. Looking back, it may have been better to just do a `get_or_create` and listen out for the `created` value. I am considering this now because I can't distinguish between `IntegrityError`s as a consequence of the uniqueness constraint or if it was to do with another issue which would need more complex resolution (such as primary key sequence going out of sync or something else). This would essentially be swallowing all `IntegrityError`s, which isn't great.
14. I added a hard limit of 100 records per response for the list view in an attempt to address requirement 2(a), but a better approach might be to use `rest_framework.pagination.LimitOffsetPagination` and then just allow the frontend to request the limit that it wants by appending `?limit=N` to the end of the URL.
15. The API endpoints are accessible via the web without limitation at the moment - that is, navigating to `/api/v1/` would list all of the endpoints. This would be something we would block in production (along with the entire DRF web interface).

