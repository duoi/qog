import random
import string

import factory
from factory.faker import faker

from address.models import Address, Country
from user.factories import UserFactory


def coin_toss():
    return random.randint(0,1)

class AddressFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Address

    @factory.lazy_attribute
    def building_name(self):
        if coin_toss():
            return faker.Faker().company()

        return ''

    @factory.lazy_attribute
    def unit_number(self):
        unit = ""
        if coin_toss():
            unit += str(random.randint(10,99))

            if coin_toss():
                unit += random.choice(string.ascii_uppercase)

        return unit

    @factory.lazy_attribute
    def street_number(self):
        street_number = ""
        if coin_toss():
            street_number += str(random.randint(1000, 9999))

            if coin_toss():
                street_number += f" {random.choice(['N', 'S', 'W', 'E'])}"

        return street_number

    @factory.lazy_attribute
    def street_name(self):
        return faker.Faker().first_name()

    @factory.lazy_attribute
    def street_type(self):
        return random.choice(['Ct', 'Rd', 'St', 'Dr', 'Cres'])

    @factory.lazy_attribute
    def suburb(self):
        return faker.Faker().city()

    @factory.lazy_attribute
    def state(self):
        return ''.join([random.choice(string.ascii_uppercase) for n in range(2)])

    @factory.lazy_attribute
    def postcode(self):
        return random.randint(1000, 9999)

    @factory.lazy_attribute
    def country(self):
        return random.choice(Country.objects.all())

    user = factory.SubFactory(UserFactory)
