import random

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from address.factories import AddressFactory
from user.factories import UserFactory


class Command(BaseCommand):
    help = 'Create some default data to ease testing'

    def add_arguments(self, parser):
        # Positional arguments
        parser.add_argument('username', nargs='?', default=None)
        parser.add_argument('range', nargs='?', default=None, type=int)

    def create_user(self, **params):
        user = UserFactory(
            **params
        )
        user.set_password('a')

        # to allow this user to log into django admin and modify stuff
        user.is_superuser = True
        user.is_staff = True
        user.save()

        print(f"--- Created user with username: {user.username}")
        print(f"--- With the password: a")

        return user


    def handle(self, *args, **options):
        user = None
        if options['username']:
            try:
                user = get_user_model().objects.get(
                    username=options['username']
                )
            except get_user_model().DoesNotExist:
                user = self.create_user(username=options['username'])

        if not user:
            user = self.create_user()

        number_of_addresses = options['range'] or random.randint(000,200)

        for i in range(number_of_addresses):
            AddressFactory(user=user)

        print(f"--- Added {number_of_addresses} addresses")
