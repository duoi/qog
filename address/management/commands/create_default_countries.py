from django.core.management.base import BaseCommand

from address.models import Country


class Command(BaseCommand):
    help = 'Create default countries'

    def handle(self, *args, **options):
        default_countries = ['Netherlands', 'Australia', 'United Kingdom', 'Japan']

        for country in default_countries:
            country, created = Country.objects.get_or_create(name=country)
            if created:
                print(f"--- Added country {country}")
