# Generated by Django 3.2.6 on 2021-08-29 12:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True, help_text='The date this object was created')),
                ('date_updated', models.DateTimeField(auto_now=True, help_text='The date this object was updated')),
                ('name', models.TextField(help_text='Name of the country', unique=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_now_add=True, help_text='The date this object was created')),
                ('date_updated', models.DateTimeField(auto_now=True, help_text='The date this object was updated')),
                ('building_name', models.TextField(blank=True, help_text='Name of the building, if any')),
                ('unit_number', models.TextField(blank=True, help_text='Unit number of the address (i.e A, 3C), if any')),
                ('street_number', models.TextField(blank=True, help_text='Street number of the address (i.e 45, 203), if any')),
                ('street_name', models.TextField(help_text='Name of the street for this address')),
                ('street_type', models.TextField(blank=True, help_text='The type of street (Place, Street, Road)')),
                ('suburb', models.TextField(help_text='Suburbs are a catch-all reference for all local government areas in Australian English')),
                ('state', models.TextField(help_text='The province, state, department or similar for this location')),
                ('postcode', models.TextField()),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='addresses', to='address.country')),
                ('user', models.ForeignKey(help_text='The user that this address is linked to', on_delete=django.db.models.deletion.PROTECT, related_name='addresses', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('building_name', 'unit_number', 'street_number', 'street_name', 'street_type', 'suburb', 'state', 'country', 'postcode', 'user')},
            },
        ),
    ]
