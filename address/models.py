from django.db import models

from qogita_project.common.mixins import TimeStampMixin


class Country(TimeStampMixin):
    name = models.TextField(
        help_text="Name of the country",
        unique=True
    )

    def __str__(self):
        return self.name


class Address(TimeStampMixin):
    """
    I appreciate there are more possible address combinations than the fields below,
    for this exercise I'm assuming these are the individual fields we want to capture.

    Implementation could just have the user write out most of this as line 1/line 2/line 3,
    or external data providers might break it down individually and we'd need to capture
    more fields.

    The real question is if we want to actually delete these - it might be more prudent to
    add an `is_active` boolean and just archive the deleted addresses instead of actually
    deleting them, keeping the database complete for auditing/compliance and avoiding
    hacking things around to access invoices and similar retroactively.
    """
    building_name = models.TextField(
        blank=True,
        help_text="Name of the building, if any"
    )
    unit_number = models.TextField(
        blank=True,
        help_text="Unit number of the address (i.e A, 3C), if any"
    )
    street_number = models.TextField(
        blank=True,
        help_text="Street number of the address (i.e 45, 203), if any"
    )
    street_name = models.TextField(
        help_text="Name of the street for this address"
    )
    street_type = models.TextField(
        blank=True,
        help_text="The type of street (Place, Street, Road)"
    )
    suburb = models.TextField(
        help_text="Suburbs are a catch-all reference for all local government areas in Australian English"
    )

    # state and suburb can probably use django-cities-light or have their own defined
    # table with a FK pointing to it, but for this exercise i'm leaving them as free text fields.
    # i'm defining country as a foreign key just to demonstrate the required optimisations in the API
    # query
    state = models.TextField(
        help_text="The province, state, department or similar for this location"
    )

    # postcodes can have letters or start with 0 so setting them as strings saves
    # hassles later on
    postcode = models.TextField()

    # these two are set to protect to try to prevent accidental deletion
    country = models.ForeignKey(
        'address.Country',
        on_delete=models.PROTECT,
        related_name="addresses"
    )
    user = models.ForeignKey(
        'auth.User',
        on_delete=models.PROTECT,
        help_text="The user that this address is linked to",
        related_name='addresses'
    )

    @property
    def address_line(self):
        """
        This is a rough 2-minute implementation of how an Australian address
        would be formatted. I appreciate that this would change between
        country to country.

        :return string:
        """
        first_line_items = ["building_name"] if self.building_name else []
        second_line_items = ["unit_number", "street_number", "street_name", "street_type"]
        third_line_items = ["suburb", "state", "postcode"]
        forth_line_items = ["country"]

        address_string = []
        for block in [first_line_items, second_line_items, third_line_items, forth_line_items]:
            if not block:
                continue

            address_string.append(" ".join(
                [str(getattr(self, item)) for item in block if getattr(self, item)]
            ))

        return ", ".join(address_string)

    def __str__(self):
        return self.address_line

    class Meta:
        # enforce the address constraints on the database level
        unique_together = [
            'building_name',
            'unit_number',
            'street_number',
            'street_name',
            'street_type',
            'suburb',
            'state',
            'country',
            'postcode',
            'user'
        ]
