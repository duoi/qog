from django.db import IntegrityError
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from address.models import Address, Country


class AddressSerializer(serializers.Serializer):
    id = serializers.IntegerField(
        read_only=True
    )
    addressLine = serializers.CharField(
        help_text="A one-line Australian address format of the address",
        read_only=True,
        source="address_line"
    )

    buildingName = serializers.CharField(
        required=False,
        source="building_name",
        help_text="Name of the building, if any"
    )
    unitNumber = serializers.CharField(
        required=False,
        source="unit_number",
        help_text="Unit number of the address (i.e A, B), if any"
    )
    streetNumber = serializers.CharField(
        required=False,
        source="street_number",
        help_text="Street number of the address (i.e 104, 203), if any"
    )
    streetType = serializers.CharField(
        required=False,
        source="street_type",
        help_text="The type of street (Place, Street, Road)"
    )
    streetName = serializers.CharField(
        required=True,
        source="street_name",
        help_text="Name of the street for this address"
    )
    suburb = serializers.CharField(
        required=True,
        help_text="The name of the local government area"
    )
    state = serializers.CharField(
        required=True,
        help_text="The province, state, department or similar for this location"
    )
    country = serializers.SlugRelatedField(
        queryset=Country.objects.all(),
        slug_field="name",
        required=True
    )
    postcode = serializers.CharField(
        required=True
    )

    def create(self, validated_data):
        """
        This is a simple implementation of the duplicate checking - if we
        wanted to provide more value we could just do a get_or_create and
        return the ID of the relevant existing address.

        :param validated_data:
        :return:
        """
        try:
            address = Address.objects.create(
                user=self.context['request'].user,
                **validated_data
            )
        except IntegrityError:
            raise ValidationError('This address already exists on your account.')

        return address

    def update(self, instance, validated_data):
        for key, val in validated_data.items():
            if hasattr(instance, key):
                setattr(instance, key, val)

        try:
            instance.save(force_update=True)
        except IntegrityError:
            raise ValidationError('This address already exists on your account.')

        return instance
