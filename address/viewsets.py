from rest_framework import response, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated

from address.models import Address
from address.serializers import AddressSerializer
from qogita_project.common.pagination import PaginationRestrictedTo100
from qogita_project.common.viewsets import BaseViewset


class AddressViewset(BaseViewset):
    serializer_class = AddressSerializer
    pagination_class = PaginationRestrictedTo100
    permission_classes = [IsAuthenticated]

    def get_params(self):
        """
        This is a crude way of handling filtering, there are packages
        that deal with this like django-filter which is recommended by
        DRF to use. I put this implementation as part of this exercise
        to demonstrate that it can be implemented if we wanted to do so.

        This doesn't provide functionality for complex filtering, just
        direct attribute/value matching and explicitly removes things
        outside of that scope.

        This also expects the model-type snake_case name of the attribute
        rather than the camelCase version displayed in the API.

        I could have just set the fields on the serializer to all be
        required=False and then added some manual validation logic,
        that way I could have passed this dictionary into the serializer
        and received a much cleaner payload (that would have de-camel
        cased the params etc) but I felt that it was overkill for this.

        :return dict:
        """

        # convert it into a dict so it can become mutable
        valid_params = dict(self.request.query_params)

        for param in self.request.query_params:
            # validate that the param actually contains a value
            if not self.request.query_params.get(param):
                valid_params.pop(param)
                continue

            # validate that the attribute exists on the model, remove `user` if passed in
            if not hasattr(Address, param) or param.lower() == 'user':
                valid_params.pop(param)
                continue

            # because we have `country` as a FK, change the key to its named attribute
            if param == 'country':
                valid_params.update({'country__name': self.request.query_params.get(param)})
                valid_params.pop(param)
                continue

            # drf puts it into a list, so extract that value and reinsert it
            valid_params.pop(param)
            valid_params.update({param: self.request.query_params.get(param)})

        return valid_params

    def get_queryset(self):
        """
        We have a fairly flat single Address model here,
        if we were dealing with a more normalised
        database structure we could add whatever
        joins we need prior to serialisation for
        performance, i.e limiting fields with `values`,
        or prefetching attributes with `select_related`
        or prejoining (on the python level) with
        `prefetch_related` and so on.

        I've created a country model since writing this
        to demonstrate at least `select_related` being
        used.

        :return queryset:
        """

        valid_params = self.get_params()

        return Address.objects.filter(
            user=self.request.user,
            **valid_params
        ).select_related(
            'country'
        )

    def validate_object_deletion(self):
        """
        This is somewhat crude but adding bulk deletion to a list view is something that
        DRF doesn't like very much. I could place some of this logic elsewhere but it's
        not lengthy and i'd prefer visibility over this portion of (destructive) code.

        :return queryset:
        """
        id_list = self.request.data.get('delete')

        if not id_list:
            raise ValidationError("You must pass in an array of primary keys attached to a `delete` key")

        if not isinstance(id_list, list):
            raise ValidationError("This endpoint can only accept arrays of primary keys")

        address_objects = Address.objects.filter(
            id__in=id_list,
            user=self.request.user
        )

        # in case someone passes in ids of objects that does not belong to them,
        # or if they have previously deleted those objects and the request was
        # accidentally sent again.
        if len(address_objects) != len(id_list):
            raise ValidationError("Invalid ID list passed in request")

        return address_objects

    @action(methods=['delete'], detail=False)
    def delete(self, request):
        address_objects = self.validate_object_deletion()
        address_objects.delete()

        return response.Response(status=status.HTTP_204_NO_CONTENT)
