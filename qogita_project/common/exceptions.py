from rest_framework.exceptions import APIException

class AuthenticationError(APIException):
    status_code = 403
    default_code = 'auth'
    default_detail = 'Invalid login credentials, please check your username and password'
