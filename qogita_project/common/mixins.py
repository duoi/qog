from django.db import models


class TimeStampMixin(models.Model):
    date_created = models.DateTimeField(
        help_text="The date this object was created",
        auto_now_add=True
    )
    date_updated = models.DateTimeField(
        help_text="The date this object was updated",
        auto_now=True
    )

    class Meta:
        abstract = True
