from django.contrib.auth.base_user import AbstractBaseUser

from qogita_project.common.mixins import TimeStampMixin


class User(AbstractBaseUser):
    """
    Subclassing the auth model here because it's best practice.
    Trying to extend or change the auth model (say, model methods)
    after a project has been in production is somewhere between
    "hard" and "impossible."

    I was initially going to force the usernames to become UUIDs
    and then change the user creation to just require an email
    and a password, but decided its significantly more than what
    this exercise was after.
    """
    pass
