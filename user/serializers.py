from django.contrib.auth import authenticate
from knox.models import AuthToken
from rest_framework import serializers

from qogita_project.common.exceptions import AuthenticationError


class AuthenticationSerializer(serializers.Serializer):
    username = serializers.CharField(
        required=True,
        write_only=True
    )
    password = serializers.CharField(
        required=True,
        write_only=True
    )

    def validate(self, attrs):
        super().validate(attrs)

        user = authenticate(**attrs)

        if not user:
            raise AuthenticationError()

        return user

    def get_token(self, user):
        _, token = AuthToken.objects.create(user=user)
        return token
