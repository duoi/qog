from knox.models import AuthToken
from rest_framework import viewsets, mixins, response, serializers
from rest_framework.permissions import AllowAny, IsAuthenticated

from user.serializers import AuthenticationSerializer


class AuthenticationViewset(viewsets.GenericViewSet, mixins.CreateModelMixin):
    serializer_class = AuthenticationSerializer
    permission_classes = [AllowAny]

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = serializer.get_token(serializer.validated_data)

        # you can probably do something like the below to set it as a cookie, not sure
        # how well it would play with drf responses though. if this is the right
        # decision it might be worth returning a vanilla HTTP Response rather than
        # having this as a drf viewset, but for the purpose of this exercise it does
        # what is expected.

        # response.Response.set_cookie(
        #     key="token",
        #     value=f"Token {token}"
        # )
        return response.Response({"token": f"Token {token}"})


class LogoutViewset(viewsets.GenericViewSet, mixins.CreateModelMixin):
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.Serializer

    def create(self, request, *args, **kwargs):
        """
        As you can see, I'm deleting all tokens rather than just the one
        used to make the request. This would force logout the user across
        all devices. This isn't great and a better approach would probably
        be to just invalidate the single token used to make the request.

        I'm also bulk deleting the tokens rather than just disabling them.
        I believe disabling them, however, might be the more prudent
        decision for auditing purposes.

        :param request:
        :param args:
        :param kwargs:
        :return 200:
        """
        AuthToken.objects.filter(
            user=request.user
        ).delete()

        # I'm intentionally not terminating sessions, just tokens, so that
        # it doesn't boot you out of Django Admin or the web interface for
        # DRF. To end that session it would be a matter of adding
        # `logout(request)`.

        return response.Response({'Logged out successfully'}, status=200)
